import * as mongoose from "mongoose";
import validator from 'validator';
import { userRoles } from "./user.enums";
const types = mongoose.SchemaTypes;

export const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
        index: true,
        trim: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Invalid Email');
            }
        }
    },
    password: {
        type: String,
        required: true,
        trim: true,
        minlength: 8,
        select:false,
        validate(value) {
            if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
                throw new Error('Password must contain at least one letter and one number');
            }
        },
        private: true,
    },
    role: {
        type: String,
        enum: userRoles,
        required: true,
    },
    // student specific
    accepted: {
        type: Boolean,
        required: function () {
            return this.role === "Student" ? true : false
        }
    },
    // student specific
    parent_contact: {
        type: String,
        required: function () {
            return this.role === "Student" ? true : false
        }
    },
    // student specific
    parent_name: {
        type: String,
        required: function () {
            return this.role === "Student" ? true : false
        }
    },
    contact: {
        type: String,
        required: function () {
            return this.role !== "Student" ? true : false
        }
    },
    address: {
        type: String,
        required: true,
    },
    country: {
        type: String,
        required: true,
    },
    postCode: {
        type: String,
        required: true,
    },
    // student specific
    batch: {
        type: types.ObjectId,
        // required: function () {
        //     return this.role === "Student" ? true : false
        // },
    },
    // teacher specific
    batches: {
        type: [types.ObjectId],
        required: function () {
            return this.role.toString() === "Teacher"
        },
        // validate: {
        //     validator: (v) => {
        //         return v.length > 1;
        //     },
        //     message: props => "minimum 1 batch required"
        // },

    },
    blood_group: {
        type: String,
        required: true,
    },
    // teacher specific
    subject: {
        type: String,
        required: function () {
            return this.role === "Teacher" ? true : false
        }
    },
    fees_paid_till: {
        type: Number,
        required: function () {
            return this.role === "Student" ? true : false
        }
    },
    photo: {
        type: String,
        default: ""
    },
});