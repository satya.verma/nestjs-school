import * as mongoose from "mongoose";

export interface User extends mongoose.Document {
    readonly name: string;
    readonly email: string;
    readonly password: string;
    readonly role: string;
    readonly accepted: boolean;
    readonly parent_contact: string;
    readonly parent_name: string;
    readonly contact: string;
    readonly address: string;
    readonly country: string;
    readonly postCode: string;
    readonly batch: mongoose.Types.ObjectId;
    readonly batches: [mongoose.Types.ObjectId];
    readonly blood_group: string;
    readonly subject: string;
    readonly fees_paid_till: number;
    readonly photo: string;
}