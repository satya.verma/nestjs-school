import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Put, Req, Request, Res, UnauthorizedException, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { ValidateObjectId } from 'src/batches/shared/pipes/validate-object-id.pipes';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { CreateUserDTO, UpdateUserDTO } from './user.dto';
import { UserService } from './user.service';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { AdminAuthGuard } from './guards/admin-auth.guard';
import { Types } from 'mongoose';
import { BatchService } from 'src/batches/batch.service';

@Controller("user")
export class UserController {
    constructor(private readonly userService: UserService) { }

    @Post('signup')
    @UsePipes(new ValidationPipe())
    async signup(
        @Res() res,
        @Body() createUserDTO: CreateUserDTO,
    ) {
        const newUser = await this.userService.addUser(createUserDTO);
        return res.status(HttpStatus.OK).json({
            message: "User successfully added",
            data: newUser
        });
    }

    @UseGuards(LocalAuthGuard)
    @Post('signin')
    async signIn(
        @Request() req
    ) {
        return await this.userService.signIn(req.user);
    }

    @Get('single/:id')
    async getUserById(
        @Res() res,
        @Param('id', new ValidateObjectId()) userId
    ) {
        const user = await this.userService.getUserById(userId);
        return res.status(HttpStatus.OK).json({
            data: user
        });
    }

    @UseGuards(JwtAuthGuard)
    @Get('all')
    async getAllUsers(
        @Res() res,
    ) {
        const users = await this.userService.getAllUsers();
        return res.status(HttpStatus.OK).json({
            data: users
        });
    }

    @Get('admins')
    async getAllAdmins(
        @Res() res,
    ) {
        const admins = await this.userService.getAllAdmins();
        return res.status(HttpStatus.OK).json({
            data: admins
        });
    }

    @Get('teachers')
    async getAllTeachers(
        @Res() res,
    ) {
        const teachers = await this.userService.getAllTeachers();
        return res.status(HttpStatus.OK).json({
            data: teachers
        });
    }

    @Get('students')
    async getAllStudents(
        @Res() res,
    ) {
        const students = await this.userService.getAllStudents();
        return res.status(HttpStatus.OK).json({
            data: students
        });
    }

    @Get('accepted_students')
    async getAcceptedStudents(
        @Res() res,
    ) {
        const students = await this.userService.getAcceptedStudents();
        return res.status(HttpStatus.OK).json({
            data: students
        });
    }

    @Get('non_accepted_students')
    async getNonAcceptedStudents(
        @Res() res,
    ) {
        const students = await this.userService.getNonAcceptedStudents();
        return res.status(HttpStatus.OK).json({
            data: students
        });
    }

    @Get('batch_students')
    async getStudentsInBatch(
        @Res() res,
        @Param('id', new ValidateObjectId()) batchId
    ) {
        const students = await this.userService.getAllStudentsInBatch(batchId);
        return res.status(HttpStatus.OK).json({
            data: students
        });
    }

    @UseGuards(JwtAuthGuard)
    @Patch('single/:id')
    @UsePipes(new ValidationPipe())
    async updateUser(
        @Res() res,
        @Param('id', new ValidateObjectId()) userId,
        @Body() updateUserDTO: UpdateUserDTO,
        @Request() req
    ) {
        if (req.user._id === userId) {
            const updatedUser = await this.userService.updateUser(userId, updateUserDTO);
            return res.status(HttpStatus.OK).json({
                message: "User successfully updated",
                data: updatedUser
            });
        }
        throw new UnauthorizedException("Access Unauthorized");
    }

    @UseGuards(AdminAuthGuard)
    @Delete(':id')
    async deleteUser(
        @Res() res,
        @Param('id', new ValidateObjectId()) userId,
        @Request() req
    ) {
        const data = await this.userService.deleteUser(userId);
        return res.status(HttpStatus.OK).json({
            message: "User successfully deleted",
            data: data
        });
    }

    @UseGuards(AdminAuthGuard)
    @Patch('accept_registration')
    async registerStudent(
        @Res() res,
        @Body('batch_id', new ValidateObjectId()) batchId,
        @Body('user_id', new ValidateObjectId()) userId,
        @Request() req,
    ) {
        const userAccepted = await this.userService.acceptRegistration(userId, batchId);
        return res.status(HttpStatus.OK).json({
            message: "User registration accepted",
            data: { userId: userAccepted }
        });
    }

    @Get()
    // @Header('Content-Type', 'text/html')
    getHello(): {} {
        return this.userService.getHello();
    }

}