import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { MongooseModule } from '@nestjs/mongoose';

import { UserController } from './user.controller';
import { UserSchema } from './user.model';
import { UserService } from './user.service';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtStrategy } from './strategies/jwt.strategy';
import { AdminStrategy } from './strategies/admin.strategy';
import { BatchModule } from 'src/batches/batch.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: 'src/.env',
        }),
        MongooseModule.forFeature([{ name: "User", schema: UserSchema }]),
        PassportModule,
        JwtModule.register({
            secret: process.env.JWT_SECRET,
            signOptions: { expiresIn: '360s' }
        }),
        BatchModule
    ],
    controllers: [UserController],
    providers: [UserService, LocalStrategy, JwtStrategy, AdminStrategy],
    exports: [UserService],
})
export class UserModule { }