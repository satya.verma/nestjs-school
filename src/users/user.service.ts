import { NotAcceptableException } from '@nestjs/common';
import { BadRequestException, ConflictException, HttpException, HttpStatus, Injectable, NotFoundException, Type } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { Model, Types } from 'mongoose';
import { BatchService } from 'src/batches/batch.service';
import { CreateUserDTO, UpdateUserDTO } from './user.dto';
import { User } from './user.interface';

@Injectable()
export class UserService {

    constructor(
        @InjectModel("User") private readonly userModel: Model<User>,
        private jwtService: JwtService,
        private batchService: BatchService
    ) { }

    // add user
    async addUser(createUserDTO: CreateUserDTO): Promise<User> {
        try {
            const doesUserExist = await this.findUserByEmail(createUserDTO.email);
            if (doesUserExist) {
                throw new ConflictException("email already exist");
            }
            const hashedPassword = await bcrypt.hash(createUserDTO.password, 10);
            const user = await new this.userModel({ ...createUserDTO, password: hashedPassword }).save();
            return user;
        } catch (e) {
            throw new BadRequestException(e.message.toString());
        }
    }

    // sign in
    async signIn(user: User) {
        console.log(user.email); console.log(user._id);
        const payload = { email: user.email, sub: user._id };
        return {
            accessToken: this.jwtService.sign(payload),
        };
    }

    // validate user
    async validateUser(email: string, password: string): Promise<User> {
        const user = await this.userModel.findOne({ email }).select('+password');
        if (!user) {
            return null;
        }
        const valid = await bcrypt.compare(password, user.password);
        if (valid) {
            return user;
        }
        return null;
    }

    // validate admin
    async validateAdmin(id: Types.ObjectId): Promise<User> {
        const user = await this.userModel.findOne({ _id: id });
        if (!user) {
            return null;
        }
        const valid = user.role === "Admin";
        if (valid) {
            return user;
        }
        return null;
    }

    // find user by email
    async findUserByEmail(email: string): Promise<User> {
        const user = await this.userModel.findOne({ email: email });
        if (!user) {
            return null;
        }
        return user;
    }

    // find user by id
    async getUserById(userId: Types.ObjectId): Promise<User> {
        const user = await this.userModel.findOne({ _id: userId });
        if (!user) {
            throw new NotFoundException("User not found");
        }
        return user;
    }

    // get all users
    async getAllUsers(): Promise<User[]> {
        let res = await this.userModel.find();
        return res;
    }

    // get all admins
    async getAllAdmins(): Promise<User[]> {
        let res = await this.userModel.find({ role: "Admin" });
        return res;
    }

    // get all teachers
    async getAllTeachers(): Promise<User[]> {
        let res = await this.userModel.find({ role: "Teacher" });
        return res;
    }

    // get all students
    async getAllStudents(): Promise<User[]> {
        let res = await this.userModel.find({ role: "Student" });
        return res;
    }

    // get all accepted students
    async getAcceptedStudents(): Promise<User[]> {
        let res = await this.userModel.find({ role: "Student", accepted: true });
        return res;
    }

    // get all non accepted students
    async getNonAcceptedStudents(): Promise<User[]> {
        let res = await this.userModel.find({ role: "Student", accepted: false });
        return res;
    }

    // get all students in a batch
    async getAllStudentsInBatch(batchId: Types.ObjectId): Promise<User[]> {
        let res = await this.userModel.find({ role: "Student", batch: batchId });
        return res;
    }

    // update user
    async updateUser(userId: Types.ObjectId, updateUserDTO: UpdateUserDTO) {
        let existingUser: User;
        if (updateUserDTO.hasOwnProperty("email")) {
            existingUser = await this.findUserByEmail(updateUserDTO.email);
            if (existingUser) {
                throw new ConflictException("email already exist");
            }
        }
        let userObject = { ...updateUserDTO };
        if (updateUserDTO.hasOwnProperty("password")) {
            const hashedPassword = await bcrypt.hash(updateUserDTO.password, 10);
            userObject.password = hashedPassword;
        }
        const updatedUser = await this.userModel.findByIdAndUpdate(userId, userObject, { new: true });
        if (!updatedUser) {
            throw new NotFoundException("User not found");
        }
        return updatedUser;
    }

    // delete user
    async deleteUser(userId: Types.ObjectId): Promise<User> {
        let res = await this.userModel.findByIdAndRemove(userId);
        if (!res) {
            throw new NotFoundException("User not found");
        }
        return null;
    }

    // accept user registration
    async acceptRegistration(userId: Types.ObjectId, batchId: Types.ObjectId): Promise<Types.ObjectId> {
        let user = await this.getUserById(userId);
        if (user.accepted) {
            throw new NotAcceptableException("user is already registered");
        }
        let res = await this.userModel.findByIdAndUpdate(userId, { accepted: true }, { new: true });
        if (res._id) {
            await this.batchService.updateTotalStudents(batchId);
            return res._id;
        }
        throw new HttpException("failed to accept registration", HttpStatus.EXPECTATION_FAILED);
    }

    getHello(): { name: string } {
        return { name: "test" };
    }

}