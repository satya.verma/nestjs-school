import { Exclude } from "class-transformer";
import { ArrayMinSize, IsBoolean, IsEmail, IsEmpty, IsMongoId, IsNotEmpty, IsOptional, IsString, MinLength, ValidateIf } from "class-validator";
import * as mongoose from "mongoose";

export class CreateUserDTO {
    @IsNotEmpty()
    name: string;
    @IsEmail()
    email: string;
    @MinLength(8)
    password: string;
    @IsNotEmpty()
    role: string;
    @ValidateIf(properties => properties.role === "Student")
    @IsNotEmpty()
    accepted: boolean;
    @ValidateIf(properties => properties.role === "Student")
    @IsNotEmpty()
    parent_contact: string;
    @ValidateIf(properties => properties.role === "Student")
    @IsNotEmpty()
    parent_name: string;
    @ValidateIf(properties => properties.role !== "Student")
    @IsNotEmpty()
    contact: string;
    @IsNotEmpty()
    address: string;
    @IsNotEmpty()
    country: string;
    @IsNotEmpty()
    postCode: string;
    @ValidateIf(properties => properties.role === "Student")
    @IsEmpty()
    batch: mongoose.Types.ObjectId;
    @ValidateIf(properties => properties.role === "Teacher")
    @ArrayMinSize(1, {
        message: "minimum 1 batch required"
    })
    batches: [mongoose.Types.ObjectId];
    @IsNotEmpty()
    blood_group: string;
    @ValidateIf(properties => properties.role === "Teacher")
    @IsNotEmpty()
    subject: string;
    @ValidateIf(properties => properties.role === "Student")
    @IsNotEmpty()
    fees_paid_till: number;
    photo: string;


}

export class UpdateUserDTO {
    @IsNotEmpty()
    @IsOptional()
    name: string;
    @IsEmail()
    @IsOptional()
    email: string;
    @MinLength(8)
    @IsOptional()
    password: string;
    @IsNotEmpty()
    @IsOptional()
    role: string;
    accepted: boolean;
    @ValidateIf(properties => properties.role === "Student")
    @IsNotEmpty()
    @IsOptional()
    parent_contact: string;
    @ValidateIf(properties => properties.role === "Student")
    @IsNotEmpty()
    @IsOptional()
    parent_name: string;
    @ValidateIf(properties => properties.role !== "Student")
    @IsNotEmpty()
    @IsOptional()
    contact: string;
    @IsNotEmpty()
    @IsOptional()
    address: string;
    @IsNotEmpty()
    @IsOptional()
    country: string;
    @IsNotEmpty()
    @IsOptional()
    postCode: string;
    @ValidateIf(properties => properties.role === "Student")
    @IsString()
    @IsOptional()
    @IsMongoId()
    @MinLength(2, {
        message: "batch is required"
    })
    batch: mongoose.Types.ObjectId;
    @ValidateIf(properties => properties.role === "Teacher")
    @IsOptional()
    @ArrayMinSize(1, {
        message: "minimum 1 batch required"
    })
    @IsOptional()
    batches: [mongoose.Types.ObjectId];
    @IsNotEmpty()
    @IsOptional()
    blood_group: string;
    @ValidateIf(properties => properties.role === "Teacher")
    @IsNotEmpty()
    @IsOptional()
    subject: string;
    fees_paid_till: number;
    photo: string;
}