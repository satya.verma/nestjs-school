import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './users/user.module';
import { BatchModule } from './batches/batch.module';

@Module({
  imports: [
    UserModule,
    BatchModule,
    MongooseModule.forRoot('mongodb://localhost:27017/nest-school', {
      useNewUrlParser: true,
      useFindAndModify: true,
      useCreateIndex: true,
      useUnifiedTopology:true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
