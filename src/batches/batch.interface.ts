import { Document } from "mongoose";

export interface Batch extends Document {
    readonly id: string,
    readonly batch_name: string,
    readonly subjects: [string],
    readonly total_students: number,
    readonly batch_fee: number,
}