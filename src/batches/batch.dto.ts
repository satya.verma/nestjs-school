import { ArrayMinSize, IsArray, IsOptional, Min, MinLength } from "class-validator";

export class UpdateBatchDTO {
    @IsOptional()
    @MinLength(1)
    batch_name: string;
    @IsOptional()
    @IsArray()
    @ArrayMinSize(5)
    subjects: [string];
    total_students: number;
    batch_fee: number;
}

export class CreateBatchDTO {
    @MinLength(1)
    batch_name: string;
    @IsArray()
    @ArrayMinSize(5)
    subjects: [string];
    @Min(0)
    batch_fee: number;
}


