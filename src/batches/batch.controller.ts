import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Patch, Post, Res, UsePipes, ValidationPipe } from "@nestjs/common";
import { BatchService } from "./batch.service";
import * as mongoose from "mongoose";
import { ValidateObjectId } from "./shared/pipes/validate-object-id.pipes";
import { CreateBatchDTO, UpdateBatchDTO } from "./batch.dto";

@Controller('/batch')
export class BatchController {
    constructor(private readonly batchService: BatchService) { }

    // add batch
    @Post()
    @UsePipes(new ValidationPipe())
    async addBatch(
        @Res() res,
        @Body() createBatchDTO: CreateBatchDTO,
    ) {
        const newBatch = await this.batchService.addBatch(createBatchDTO);
        return res.status(HttpStatus.OK).json({
            message: "Batch has been successfully added",
            data: newBatch
        });
    }

    // get batch by id
    @Get('single/:id')
    async getBatchById(
        @Res() res,
        @Param('id', new ValidateObjectId()) batchId
    ) {
        const batch = await this.batchService.findbatchById(batchId);
        return res.status(HttpStatus.OK).json({
            data: batch
        });
    }

    // get batch by id
    @Get('total_students')
    async getTotalStudents(
        @Res() res,
    ) {
        const totalStudents = await this.batchService.getTotalStudentsInSchool();
        return res.status(HttpStatus.OK).json({
            data: totalStudents
        });
    }

    // get all batches
    @Get('batches')
    async getAllBatches(
        @Res() res,
    ) {
        const batches = await this.batchService.findAllBatches();
        return res.status(HttpStatus.OK).json({
            data: batches
        });
    }

    // update batch by id
    @Patch(':id')
    @UsePipes(new ValidationPipe())
    async updateBatch(
        @Res() res,
        @Param('id', new ValidateObjectId()) batchId,
        @Body() updateBatchDTO: UpdateBatchDTO
    ) {
        const updatedBatch = await this.batchService.updateBatch(batchId, updateBatchDTO);
        return res.status(HttpStatus.OK).json({
            message: "Batch successfully updated",
            data: updatedBatch
        });
    }

    // delete batch
    @Delete(':id')
    async deleteBatch(
        @Res() res,
        @Param('id', new ValidateObjectId()) batchId
    ) {
        const data = await this.batchService.deleteBatch(batchId);
        return res.status(HttpStatus.OK).json({
            message: "Batch has been successfully deleted",
            data: data
        });
    }

    @Get()
    getBatchHello(): string {
        return this.batchService.getBatchHello();
    }

}