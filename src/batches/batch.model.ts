import * as mongoose from "mongoose";
import { subjects1 } from "./batch.enums";

export const BatchSchema = new mongoose.Schema({
    batch_name: {
        type: String,
        required: [true, "batch_name required"],
        index: true,
        unique: true,
    },
    subjects: {
        type: [String],
        required: true,
        validate: {
            validator: (v) => {
                return v.length > 4
            },
            message: props => "minimum 5 subjects required"
        }
    },
    total_students: {
        type: Number,
        required: true,
        default: 0,
    },
    batch_fee: {
        type: Number,
        required: true,
    },
});