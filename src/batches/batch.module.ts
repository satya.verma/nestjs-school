import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { BatchController } from "./batch.controller";
import { BatchSchema } from "./batch.model";
import { BatchService } from "./batch.service";

@Module({
    imports: [MongooseModule.forFeature([{ name: "Batch", schema: BatchSchema }])],
    controllers: [BatchController],
    providers: [BatchService],
    exports:[BatchService]
})
export class BatchModule { }