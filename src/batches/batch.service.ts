import { BadRequestException, ConflictException, Injectable, NotFoundException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CreateBatchDTO, UpdateBatchDTO } from "./batch.dto";
import { Batch } from "./batch.interface";

@Injectable()
export class BatchService {

    constructor(@InjectModel("Batch") private readonly batchModel: Model<Batch>) { }

    // add batch
    async addBatch(createBatchDTO: CreateBatchDTO): Promise<Batch> {
        try {
            const doesBatchExist = await this.findBatchByName(createBatchDTO.batch_name);
            if (doesBatchExist) {
                throw new ConflictException("batch_name already exist");
            }
            const res = await new this.batchModel(createBatchDTO).save();
            return res;
        } catch (e) {
            throw new BadRequestException(e.message.toString());
        }
    }

    // find batch by id
    async findbatchById(batchId): Promise<Batch> {
        const batch = await this.batchModel.findById(batchId);
        if (!batch) {
            throw new NotFoundException("Batch Not Found");
        }
        return batch;
    }

    // find batch by name
    async findBatchByName(batchName): Promise<Batch> {
        const batch = await this.batchModel.findOne({ batch_name: batchName });
        if (!batch) {
            return null;
            // throw new NotFoundException("no batch exist for the name");
        }
        return batch;
    }

    // find all batches
    async findAllBatches(): Promise<Batch[]> {
        return await this.batchModel.find().exec();
    }

    // get total no of students in all batches
    async getTotalStudentsInSchool(): Promise<Number> {
        let res = await this.batchModel.find();
        const totalStudents = res.reduce((accumulator, currentBatch) => {
            return accumulator += currentBatch.total_students;
        }, 0)
        return totalStudents;
    }

    // update batch 
    async updateBatch(batchId, updateBatchDTO: UpdateBatchDTO): Promise<Batch> {
        let existingBatch: Batch;
        if (updateBatchDTO.hasOwnProperty("batch_name")) {
            existingBatch = await this.findBatchByName(updateBatchDTO.batch_name);
            if (existingBatch) {
                throw new ConflictException("batch_name already exist");
            }
        }
        const updatedBatch = await this.batchModel.findByIdAndUpdate(batchId, updateBatchDTO, { new: true });
        if (!updatedBatch) {
            throw new NotFoundException("Batch not found");
        }
        return updatedBatch;
    }

    // delete batch
    async deleteBatch(batchId): Promise<Batch> {
        let res = await this.batchModel.findByIdAndRemove(batchId);
        if (!res) {
            throw new NotFoundException("Batch not found");
        }
        return null;
    }

    // test batch module
    getBatchHello() {
        return "Hello from Batch";
    }

    // update total students in batch
    async updateTotalStudents(batchId): Promise<number> {
        let res = await this.batchModel.findByIdAndUpdate(batchId, { $inc: { total_students: 1 } }, { new: true });
        return res.total_students;
    }

}